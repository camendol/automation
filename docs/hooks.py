import shutil
import glob
import os
import mkdocs_diagrams
import subprocess

def compile_diagrams(*args, **kwargs):
    """
    Call diragrams compilation here.

    Hooks are executed after plugins, therefore the diagrams plugin won't see the
    workflow diagrams since they are copied under the mkdocs tree only upon calling this hook.
    This function is used to trigger diagrams again after the copy.
    """
    pdiagrams = mkdocs_diagrams.plugin.DiagramsPlugin()
    pdiagrams.config['max_workers'] = 5
    pdiagrams.config['file_extension'] = '.diagrams.py'
    pdiagrams.on_pre_build(*args, **kwargs)

def on_pre_build(*args, **kwargs):
    """Copy workflow related docs from automation/workflow dir."""
    for wflow in glob.glob('workflows/*'):
        if os.path.exists(f'{wflow}/docs'):
            orig_dir = f'{wflow}/docs/'
            docs_dir = f'docs/source/{wflow}/'
            # compile sphinx docs if needed
            if os.path.exists(f'{orig_dir}/Makefile'):            
                subprocess.run(['make', 'html'], cwd=orig_dir)            
            # cleanup first
            # copy workflow docs dir under mkdocs tree
            if os.path.exists(docs_dir):
                shutil.rmtree(docs_dir)
            shutil.copytree(orig_dir, docs_dir)
            
    compile_diagrams(*args, **kwargs)
