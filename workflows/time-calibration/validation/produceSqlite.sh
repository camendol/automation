#!/bin/bash

FILL=${1}
TAG=${2}
OUTDIR=${3}

PYTHON_PATH=$CMSSW_BASE/src/EcalTiming/EcalTiming/test

# Produce XML and sqlite files
python3 $PYTHON_PATH/makeTimingXML.py --tag=$TAG --calib=$OUTDIR/ecalTiming-corr_$FILL.dat --payload=$(cat $OUTDIR/tmppyload)
python3 $PYTHON_PATH/makeTimingSqlite.py --tag=$TAG --calib=$OUTDIR/ecalTiming-abs_$FILL.xml --firstRun=1

# Remove unnecessary stuff created
rm ./${TAG}_rereco_calib.db
